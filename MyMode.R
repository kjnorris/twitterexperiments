MyMode <- function(myVector) {
    uniqueValues <- unique(myVector)
    uniqueCounts <- tabulate(+match(myVector, uniqueValues))
    return(uniqueValues[which.max(uniqueCounts)])
}

addLevel <- function(x, newlevel=NULL){ 
    if(is.factor(x)) 
        return(factor(x, levels=c(levels(x), newlevel))) 
    return(x) 
} 

fillPO4 <- function(oP) {
    if (is.na(oP))
        return(NA)
    else return(42.897 + 1.293 * oP)
}

data(algae)
algae <- algae[-manyNAs(algae), ]
