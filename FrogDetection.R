library(DMwR)

data(sales)

head(sales)
summary(sales)
nlevels(sales$ID)
nlevels(sales$Prod)

length(which(is.na(sales$Quant) & is.na(sales$Val)))

table(sales$Insp)/nrow(sales)*100

totS <- table(sales$ID)
barplot(totS, main = "Transactions per salespeople", names.arg = "", 
        xlab = "Salespeople", ylab = "Amount")

totP <- table(sales$Prod)
barplot(totP, main = "Transactions per product", names.arg = "", 
        xlab = "Products", ylab = "Amount")

sales$Uprice <- sales$Val/sales$Quant
summary(sales$Uprice)

attach(sales)
upp <- aggregate(Uprice, list(Prod), median, na.rm = TRUE)
topP <- sapply(c(TRUE, FALSE), 
               function(o) upp[order(upp[, 2], decreasing = o)[1:5], 1])
colnames(topP) <- c('Expensive', 'Cheap')

topS <- sales[Prod %in% topP[1,], c("Prod", "Uprice")]
topS$Prod <- factor(topS$Prod)
boxplot(Uprice ~ Prod, data = topS, ylab = "Uprice", log = "y")

vs <- aggregate(Val, list(ID), sum, na.rm = TRUE)
scoresSs <- sapply(c(TRUE, FALSE), 
                   function(o) vs[order(vs$x, decreasing = o)[1:5], 1])
colnames(scoresSs) <- c('Most', 'Least')

sum(vs[order(vs$x, decreasing = TRUE)[1:100], 2])/sum(Val, na.rm = TRUE) *100
sum(vs[order(vs$x, decreasing = FALSE)[1:2000], 2])/sum(Val, na.rm = TRUE) *100

qs <- aggregate(Quant, list(Prod), sum, na.rm = TRUE)
scoresPs <- sapply(c(TRUE, FALSE), 
                   function(o) qs[order(qs$x, decreasing = o)[1:5], 1])
colnames(scoresPs) <- c('Most', 'Least')

sum(as.double(qs[order(qs$x, decreasing = TRUE)[1:100], 2])) / 
    sum(as.double(Quant), na.rm = TRUE) * 100
sum(as.double(qs[order(qs$x, decreasing = FALSE)[1:4000], 2])) / 
    sum(as.double(Quant), na.rm = TRUE) * 100

out <- tapply(Uprice, list(Prod=Prod), 
              function(x) length(boxplot.stats(x)$out))
out[order(out, decreasing = TRUE)[1:10]]
sum(out)
sum(out)/nrow(sales)*100
