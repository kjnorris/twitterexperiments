# Mother's Day word cloud

# Function EnsurePackage() loads a given library
# If the library is not installed, EnsurePackage will 
# install it and then load it.
EnsurePackage<-function(x)
{
    x <- as.character(x)
    if (!require(x,character.only=TRUE))
    {
        install.packages(pkgs=x,repos="http://cran.r-project.org")
        require(x,character.only=TRUE)
    }
}

# Function PrepareTwitter() loads all necessary packages
# to access twitter from R and opens the connection to the API
# Insert your Consumer Key, Consumer Secret, 
# Access Token, and Acces Token Secret where indicated
# You can create your own application at https://apps.twitter.com
PrepareTwitter <- function () {
    EnsurePackage("twitteR")
    EnsurePackage("stringr")
    EnsurePackage("tm")
    EnsurePackage("wordcloud")
    consumerKey <- "hlX4ePAuPfPeo7KEIKGeIH4Pl"
    consumerSecret <- "CpbdzOEg867bfCrPMeD2Sz185rB8JrlNxVVuEKBB2R4JvoQyzl"
    accessToken <- "97057675-9yo77cik2tKMnu4tIVceaObzkHfBf9dl7daW6IKm5"
    accessSecret <- "68xiEansC85PQXLsMJBj47sb3dxpag67W2xeZL82wvHwZ"
    setup_twitter_oauth(consumerKey, consumerSecret, accessToken, accessSecret)
}

# Search Twitter for a specified term and return a data frame
TweetFrame<-function(searchTerm, maxTweets = 50)
{
    twtList<-searchTwitter(searchTerm,n=maxTweets)
    unsortTweetList <- do.call("rbind", lapply(twtList,as.data.frame))
    return(unsortTweetList[order(as.integer(unsortTweetList$created)), ])
}

# CleanTweets() - Takes the junk out of a vector of tweet texts
CleanTweets<-function(tweets)
{
    # English only, please
    tweets <- sapply(tweets,function(row) iconv(row, "latin1", "ASCII", sub=""))
    # Remove redundant spaces
    tweets <- str_replace_all(tweets,"  "," ")
    # Get rid of URLs
    # tweets <- str_replace_all(tweets, "http://t.co/[a-z,A-Z,0-9]*","")
    tweets <- gsub('(f|ht)tp\\S+\\s*',"", tweets)
    # Take out retweet header, there is only one
    tweets <- str_replace(tweets,"RT @[a-z,A-Z]*: ","")
    # Get rid of hashtags
    tweets <- str_replace_all(tweets,"#[a-z,A-Z]*","")
    # Get rid of references to other screennames
    tweets <- str_replace_all(tweets,"@[a-z,A-Z]*","")
    return(tweets)
}

PrepareTwitter();


# Read up to 500 tweets referencing MothersDay
tweetDF <- TweetFrame('MothersDay', 500)

# Remove the extraneous text from the tweets
cleanText <- tweetDF$text
cleanText <- CleanTweets(cleanText)

# Build a corpus for further processing
# Normalize corpus - lower case, no punctuation, etc.
tweetCorpus <- Corpus(VectorSource(cleanText))
tweetCorpus <- tm_map(tweetCorpus, content_transformer(tolower))
tweetCorpus <- tm_map(tweetCorpus, removePunctuation)
tweetCorpus <- tm_map(tweetCorpus, removeNumbers)
tweetCorpus <- tm_map(tweetCorpus, removeWords, stopwords('english'))
# tweetCorpus <- tm_map(tweetCorpus, stemDocument)

# Build a term matrix from the normalized tweets
tweetTDM <- TermDocumentMatrix(tweetCorpus)
tdMatrix <- as.matrix(tweetTDM)

# Sort the term matrix in descending order
sortedMatrix <- sort(rowSums(tdMatrix), decreasing = TRUE)

# Build a data frame for the word cloud based upon term frequency
cloudFrame <- data.frame(word=names(sortedMatrix), freq=sortedMatrix)

# Set the color scheme and output the word cloud
pal <- brewer.pal(8, "Set2")
png("MothersDay.png", width = 800, height = 800,
    bg = "transparent")
wordcloud(cloudFrame$word, cloudFrame$freq, rot.per=0.20, 
          random.color=TRUE, colors=pal, scale=c(10,2),
          min.freq=1, max.words=50)
dev.off()
