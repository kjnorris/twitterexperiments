#Function EnsurePackage() loads a given library
#If the library is not installed, EnsurePackage will
#install it and then load it.
EnsurePackage<-function(x)
{
    x <- as.character(x)
    if (!require(x,character.only=TRUE))
    {
        install.packages(pkgs=x,repos="http://cran.r-project.org")
        require(x,character.only=TRUE)
    }
}

#Function PrepareTwitter() loads all necessary packages
#to access twitter from R
PrepareTwitter <- function () {
    EnsurePackage("bitops")
    EnsurePackage("RCurl")
    EnsurePackage("RJSONIO")
    EnsurePackage("twitteR")
    EnsurePackage("ROAuth")
    EnsurePackage("stringr")
    EnsurePackage("tm")
    EnsurePackage("wordcloud")
}

consumerKey <- "hlX4ePAuPfPeo7KEIKGeIH4Pl"
consumerSecret <- "CpbdzOEg867bfCrPMeD2Sz185rB8JrlNxVVuEKBB2R4JvoQyzl"
accessToken <- "97057675-9yo77cik2tKMnu4tIVceaObzkHfBf9dl7daW6IKm5"
accessSecret <- "68xiEansC85PQXLsMJBj47sb3dxpag67W2xeZL82wvHwZ"
setup_twitter_oauth(consumerKey, consumerSecret, accessToken, accessSecret)


#Cred <- OAuthFactory$new(consumerKey = "hlX4ePAuPfPeo7KEIKGeIH4Pl", 
#                         requestURL  ="https://api.twitter.com/oauth/request_token", 
#                         accessURL = "https://api.twitter.com/oauth/access_token", 
#                         authURL = "https://api.twitter.com/oauth/authorize", 
#                         needsVerifier=TRUE)
#Cred$handshake()
#registerTwitterOAuth(Cred)
tweetList <- searchTwitter("Summa_Tech", n = 50)

# Deprecated
# TweetFrame<-function(searchTerm, maxTweets)
# {
#     twtList<-searchTwitter(searchTerm,n=maxTweets)
#     return(do.call("rbind", lapply(twtList,as.data.frame)))
# }

TweetFrame<-function(searchTerm, maxTweets)
{
    twtList<-searchTwitter(searchTerm,n=maxTweets)
    unsortTweetList <- do.call("rbind", lapply(twtList,as.data.frame))
    return(unsortTweetList[order(as.integer(unsortTweetList$created)), ])
}

# CleanTweets() - Takes the junk out of a vector of
# tweet texts
CleanTweets<-function(tweets)
{
    # English only, please
    tweets <- sapply(tweets,function(row) iconv(row, "latin1", "ASCII", sub=""))
    # tweets <- sapply(tweets,function(row) iconv(row, to = "UTF-8", sub=""))
    # Remove redundant spaces
    tweets <- str_replace_all(tweets,"  "," ")
    # Get rid of URLs
    # tweets <- str_replace_all(tweets, "http://t.co/[a-z,A-Z,0-9]*","")
    tweets <- gsub('(f|ht)tp\\S+\\s*',"", tweets)
    # Take out retweet header, there is only one
    tweets <- str_replace(tweets,"RT @[a-z,A-Z]*: ","")
    # Get rid of hashtags
    tweets <- str_replace_all(tweets,"#[a-z,A-Z]*","")
    # Get rid of references to other screennames
    tweets <- str_replace_all(tweets,"@[a-z,A-Z]*","")
    return(tweets)
}

tweetDF <- TweetFrame('#climate', 500)
sorttweetDF <- tweetDF[order(as.integer(created)), ]

sortTweetList <- TweetFrame('#climate', 500)

# Build a wordcloud from my timeline
userTimeline("k_j_norris")
myTimeLine <- twListToDF(userTimeline("k_j_norris"))
myTimeLine$text <- CleanTweets(myTimeLine$text)
tweetCorpus <- Corpus(VectorSource(myTimeLine$text))
tweetCorpus <- tm_map(tweetCorpus, content_transformer(tolower))
tweetCorpus <- tm_map(tweetCorpus, removePunctuation)
tweetCorpus <- tm_map(tweetCorpus, removeNumbers)
tweetCorpus <- tm_map(tweetCorpus, removeWords, stopwords('english'))
tweetTDM <- TermDocumentMatrix(tweetCorpus)
tdMatrix <- as.matrix(tweetTDM)
sortedMatrix <- sort(rowSums(tdMatrix), decreasing = TRUE)
cloudFrame <- data.frame(word=names(sortedMatrix), freq=sortedMatrix)
pal <- brewer.pal(8, "OrRd")
wordcloud(cloudFrame$word, cloudFrame$freq, rot.per=0.20, 
          random.color=TRUE, colors=pal, scale=c(5,.25), 
          min.freq=1, max.words=30)


# Build a Word Cloud
tweetDF <- TweetFrame('MothersDay', 500)
cleanText <- tweetDF$text
cleanText <- CleanTweets(cleanText)
tweetCorpus <- Corpus(VectorSource(cleanText))
tweetCorpus <- tm_map(tweetCorpus, content_transformer(tolower))
tweetCorpus <- tm_map(tweetCorpus, removePunctuation)
tweetCorpus <- tm_map(tweetCorpus, removeNumbers)
tweetCorpus <- tm_map(tweetCorpus, removeWords, stopwords('english'))
# tweetCorpus <- tm_map(tweetCorpus, stemDocument)
tweetTDM <- TermDocumentMatrix(tweetCorpus)
# tweetDTM <- DocumentTermMatrix(tweetCorpus)
tdMatrix <- as.matrix(tweetTDM)
sortedMatrix <- sort(rowSums(tdMatrix), decreasing = TRUE)
cloudFrame <- data.frame(word=names(sortedMatrix), freq=sortedMatrix)
pal <- brewer.pal(8, "OrRd")
wordcloud(cloudFrame$word, cloudFrame$freq, rot.per=0.20, 
          random.color=TRUE, colors=pal, scale=c(5,.25),
          min.freq=1, max.words=30)
