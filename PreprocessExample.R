require(ggplot2)
require(gridExtra)
require(reshape2)

set.seed(1)
predictors = data.frame(x1 = rnorm(1000, mean = 5, sd = 2), 
                        x2 = rexp(1000, rate=10))

p1 = ggplot(predictors) + geom_point(aes(x = x1, y = x2))

temp <- melt(predictors, measured = c("x1", "x2"))
p2 = ggplot(temp) + geom_histogram(aes(x=value)) + 
    facet_grid(. ~ variable, scales = "free_x")

grid.arrange(p1, p2)

require(caret)

trans <- preProcess(predictors, c("BoxCox", "center", "scale"))
predictors_trans <- data.frame(trans = predict(trans, predictors))

p1 = ggplot(predictors_trans) + geom_point(aes(x = trans.x1, y = trans.x2))
temp <- melt(predictors_trans, measured = c("trans.x1", "trans.x2"))
p2 = ggplot(temp) + geom_histogram(aes(x=value), data = temp) + 
    facet_grid(. ~ variable, scales = "free_x")
grid.arrange(p1, p2)


# PCA Example
data(iris)
head(iris, 3)
log.ir <- log(iris[, 1:4])
ir.species <- iris[, 5]
ir.pca <- prcomp(log.ir,
                 center = TRUE,
                 scale. = TRUE) 
print(ir.pca)
plot(ir.pca, type = "l")
summary(ir.pca)
predict(ir.pca, newdata=tail(log.ir, 2))


library(ggbiplot)
g <- ggbiplot(ir.pca, obs.scale = 1, var.scale = 1, 
              groups = ir.species, ellipse = TRUE, 
              circle = TRUE)
g <- g + scale_color_discrete(name = '')
g <- g + theme(legend.direction = 'horizontal', 
               legend.position = 'top')
print(g)

require(ggplot2)

theta <- seq(0,2*pi,length.out = 100)
circle <- data.frame(x = cos(theta), y = sin(theta))
p <- ggplot(circle,aes(x,y)) + geom_path()

loadings <- data.frame(ir.pca$rotation, 
                       .names = row.names(ir.pca$rotation))
p + geom_text(data=loadings, 
              mapping=aes(x = PC1, y = PC2, label = .names, colour = .names)) +
    coord_fixed(ratio=1) +
    labs(x = "PC1", y = "PC2")
