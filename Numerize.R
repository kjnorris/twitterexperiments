# Numberize() - Gets rid of commas and other junk and 
# converts to numbers
# Assumes that the inputVector is a list of data that 
# can be treated as character strings
Numberize <- function(inputVector)
{
    # Get rid of commas
    inputVector<-str_replace_all(inputVector,",","")
    # Get rid of spaces
    inputVector<-str_replace_all(inputVector," ","")
    
    return(as.numeric(inputVector))
}